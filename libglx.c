/*
 * Copyright (c) 2013, NVIDIA CORPORATION.
 * SPDX-License-Identifier: MIT
 */

#include <GL/glx.h>
#include <GL/glxext.h>
#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xlibint.h>
#include <X11/Xproto.h>
#include <X11/Xlib-xcb.h>
#include <dlfcn.h>
#include <string.h>
#include <sys/types.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdlib.h>

#include <GL/glxproto.h>

#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <EGL/eglplatform.h>
#include <xcb/xcb.h>


#include "libglxmapping.h"
#include "libglxproto.h"
#include "libglxthread.h"

/* current version numbers */
#define GLX_MAJOR_VERSION 1
#define GLX_MINOR_VERSION 4
#define GLX_VERSION_STRING "1.4"

/* Default GLX extensions */
#define GLX_EGL_EXTENSIONS \
	"GLX_ARB_get_proc_address" \
   "GLX_ARB_multisample" \
   "GLX_EXT_swap_control" \
   "GLX_EXT_visual_info" \
   "GLX_EXT_visual_rating" \
   "GLX_SGI_make_current_read" \
   "GLX_SGI_swap_control" \
   "GLX_SGIX_fbconfig" \
   "GLX_SGIX_pbuffer" \
   "GLX_ARB_create_context " \
   "GLX_ARB_create_context_profile " \
   "GLX_ARB_get_proc_address " \
   "GLX_EXT_create_context_es_profile " \
   "GLX_EXT_create_context_es2_profile " \
   "GLX_EXT_framebuffer_sRGB" \
   "GLX_EXT_texture_from_pixmap"


/*
 * Older versions of glxproto.h contained a typo where "Attribs" was misspelled.
 * The typo was fixed in the xorgproto version of glxproto.h, breaking the API.
 * Work around that here.
 */
#if !defined(X_GLXCreateContextAttribsARB) && \
    defined(X_GLXCreateContextAtrribsARB)
#define X_GLXCreateContextAttribsARB X_GLXCreateContextAtrribsARB
#endif


/* Emulate CurrentDrawable */
EGLX_THREAD_LOCAL(currentAPIEGL, EGLenum, EGL_OPENGL_API)

/* This need to be implemented as hash table containing mappings:
 * Display to EGLDisplay
 * GLXDrawable to EGLSurface
 * GLXFBConfig to EGLConfig
 */
 extern bool set_drawable_for_surface(GLXDrawable drawable, EGLSurface surf);
 extern bool unset_drawable_for_surface(GLXDrawable drawable, EGLSurface surf);

 extern GLXDrawable get_drawable_from_surface(EGLSurface surf);
 extern EGLSurface get_surface_from_drawable(GLXDrawable drawable);
 extern EGLDisplay get_egl_display_from_xcb_connection(xcb_connection_t* c);
 #define get_egl_display_from_display(x) get_egl_display_from_xcb_connection(XGetXCBConnection(x))
 extern Display* get_display_from_egl_display(EGLDisplay display);
 extern EGLConfig get_egl_config_from_glxfbconfig(GLXFBConfig conf);
 extern GLXFBConfig get_glxfbconfig_from_egl_config(EGLConfig conf);



 /*End implemenation requirements */


static GLXContext __glxCreateContextAttribsARB(Display *dpy, GLXContext share, Bool direct, const int *glxAttribs, int* err)
{
		if(!direct)
      {
         *err = EGL_BAD_CONTEXT;
         return NULL;
      } 

		int eglAttribs[MAX_ATTRIBS + 1], egli = 0;
		for(int i = 0; i < MAX_ATTRIBS + 1; i++) eglAttribs[i] = EGL_NONE;
		bool majorVerSpecified = false, forwardCompatSpecified = false;
		int majorVer = -1;

		if(glxAttribs && glxAttribs[0] != None)
		{
			for(int glxi = 0; glxAttribs[glxi] && egli < MAX_ATTRIBS; glxi += 2)
			{
				switch(glxAttribs[glxi])
				{
					case GLX_CONTEXT_MAJOR_VERSION_ARB:
						eglAttribs[egli++] = EGL_CONTEXT_MAJOR_VERSION;
						eglAttribs[egli++] = majorVer = glxAttribs[glxi + 1];
						majorVerSpecified = true;
						break;
					case GLX_CONTEXT_MINOR_VERSION_ARB:
						eglAttribs[egli++] = EGL_CONTEXT_MINOR_VERSION;
						eglAttribs[egli++] = glxAttribs[glxi + 1];
						break;
					case GLX_CONTEXT_FLAGS_ARB:
					{
						int flags = glxAttribs[glxi + 1];
						if(flags & GLX_CONTEXT_DEBUG_BIT_ARB)
						{
							eglAttribs[egli++] = EGL_CONTEXT_OPENGL_DEBUG;
							eglAttribs[egli++] = EGL_TRUE;
						}
						if(flags & GLX_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB)
						{
							eglAttribs[egli++] = EGL_CONTEXT_OPENGL_FORWARD_COMPATIBLE;
							eglAttribs[egli++] = EGL_TRUE;
							forwardCompatSpecified = true;
						}
						if(flags & GLX_CONTEXT_ROBUST_ACCESS_BIT_ARB)
						{
							// For future expansion
							eglAttribs[egli++] = EGL_CONTEXT_OPENGL_ROBUST_ACCESS;
							eglAttribs[egli++] = EGL_TRUE;
						}
						if(flags & ~(GLX_CONTEXT_DEBUG_BIT_ARB |
							GLX_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB))
						{
                     /* BadValue */
                     *err = EGL_BAD_PARAMETER;
							return NULL;
						}
						break;
					}
					case GLX_CONTEXT_PROFILE_MASK_ARB:
						// The mask bits are the same for GLX_ARB_create_context and EGL.
						eglAttribs[egli++] = EGL_CONTEXT_OPENGL_PROFILE_MASK;
						eglAttribs[egli++] = glxAttribs[glxi + 1];
						if(glxAttribs[glxi + 1] != GLX_CONTEXT_CORE_PROFILE_BIT_ARB
							&& glxAttribs[glxi + 1]
								!= GLX_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB)
						{
                     /* GLXBadProfileARB */
                     *err = EGL_BAD_CONTEXT;
							return NULL;
						}
						break;
					case GLX_CONTEXT_RESET_NOTIFICATION_STRATEGY_ARB:
						switch(glxAttribs[glxi + 1])
						{
							case GLX_NO_RESET_NOTIFICATION_ARB:
								eglAttribs[egli++] =
									EGL_CONTEXT_OPENGL_RESET_NOTIFICATION_STRATEGY;
								eglAttribs[egli++] = EGL_NO_RESET_NOTIFICATION;
								break;
							case GLX_LOSE_CONTEXT_ON_RESET_ARB:
								eglAttribs[egli++] =
									EGL_CONTEXT_OPENGL_RESET_NOTIFICATION_STRATEGY;
								eglAttribs[egli++] = EGL_LOSE_CONTEXT_ON_RESET;
								break;
						}
						break;
					default:
						if(glxAttribs[glxi] == GLX_RENDER_TYPE
							&& glxAttribs[glxi + 1] == GLX_COLOR_INDEX_TYPE)
						{
                     /* BadMatch */
                     *err = EGL_BAD_MATCH;
							return NULL;
						}
						else if(glxAttribs[glxi] != GLX_RENDER_TYPE
							|| glxAttribs[glxi + 1] != GLX_RGBA_TYPE)
						{
                     /* BadValue */
                     *err = EGL_BAD_PARAMETER;
							return NULL;
						}
				}
			}
		}

		CARD16 minorCode = egli ? X_GLXCreateContextAttribsARB :
			X_GLXCreateNewContext;
		if(majorVerSpecified && forwardCompatSpecified && majorVer < 3)
		{
         /* BadMatch */
         *err = EGL_BAD_MATCH;
         return NULL;
		}
		try
		{
			if(!VALID_CONFIG(config))
			{
            /*GLXBadFBConfig*/
            *err = EGL_BAD_CONFIG;
				return NULL;
			}
			if(!share)
			{
				getRBOContext(dpy).createContext();
				share = (GLXContext)getRBOContext(dpy).getContext();
			}
			if(!_eglBindAPI(EGL_OPENGL_API))
				THROW("Could not enable OpenGL API");
			GLXContext ctx = (GLXContext)_eglCreateContext(EDPY, (EGLConfig)0,
				(EGLContext)share, egli ? eglAttribs : NULL);
			EGLint eglError = _eglGetError();
			// Some implementations of eglCreateContext() return NULL but do not set
			// the EGL error if certain invalid OpenGL versions are passed to the
			// function.  This is why we can't have nice things.
			if(!ctx && eglError == EGL_SUCCESS && majorVerSpecified)
				eglError = EGL_BAD_MATCH;
			if(!ctx && eglError != EGL_SUCCESS)
				throw(EGLError("eglCreateContext()", __LINE__, eglError));
			if(ctx) ctxhashegl.add(ctx, config);
			return ctx;
		}
		CATCH_EGL(minorCode)
		return 0;
}

PUBLIC XVisualInfo* glXChooseVisual(Display *dpy, int screen, int *attrib_list)
{

}


PUBLIC void glXCopyContext(Display *dpy, GLXContext src, GLXContext dst,
                    unsigned long mask)
{
   /* Cannot copy context on direct rendering, always error */
   __glXSendGlxError(dpy, X_GLXCopyContext, BadRequest, true);
}


PUBLIC GLXContext glXCreateContext(Display *dpy, XVisualInfo *vis,
                            GLXContext share_list, Bool direct)
{

}


PUBLIC GLXContext glXCreateNewContext(Display *dpy, GLXFBConfig config,
                               int render_type, GLXContext share_list,
                               Bool direct)
{

}

static GLXContext glXCreateContextAttribsARB(Display *dpy, GLXFBConfig config,
        GLXContext share_list, Bool direct, const int *attrib_list)
{

}

PUBLIC void glXDestroyContext(Display *dpy, GLXContext context)
{
   EGLContext ctx = (EGLContext)context;
   EGLDisplay edpy = get_egl_display_from_display(dpy);
   eglDestroyContext(edpy, ctx);
}


PUBLIC GLXPixmap glXCreateGLXPixmap(Display *dpy, XVisualInfo *vis, Pixmap pixmap)
{
}


PUBLIC void glXDestroyGLXPixmap(Display *dpy, GLXPixmap pix)
{
}


PUBLIC int glXGetConfig(Display *dpy, XVisualInfo *vis, int attrib, int *value)
{
}

PUBLIC GLXContext glXGetCurrentContext(void)
{
		if(!eglBindAPI(getCurrentAPIEGL()))
      {
			return (GLXContext)EGL_NO_CONTEXT;
      }
		return (GLXContext)eglGetCurrentContext();
}


PUBLIC GLXDrawable glXGetCurrentDrawable(void)
{
   return get_drawable_from_egl_surface(eglGetCurrentSurface(EGL_DRAW));
}

PUBLIC GLXDrawable glXGetCurrentReadDrawable(void)
{
   return get_drawable_from_egl_surface(eglGetCurrentSurface(EGL_READ));
}

PUBLIC Display *glXGetCurrentDisplay(void)
{
   return get_display_from_egl_display(eglGetCurrentDisplay());
}

PUBLIC Bool glXIsDirect(Display *dpy, GLXContext context)
{
   /* We can do only Direct rendering on EGL */
   return True;
}

PUBLIC Bool glXMakeCurrent(Display *dpy, GLXDrawable drawable, GLXContext context)
{
   glXMakeContextCurrent(dpy, drawable, drawable, context);
}

PUBLIC Bool glXMakeContextCurrent(Display *dpy, GLXDrawable draw,
                                  GLXDrawable read, GLXContext context)
{
   EGLDisplay edpy = get_egl_display_from_display(dpy);
   EGLSurface dsurf = get_egl_surface_from_drawable(draw);
   EGLSurface rsurf = get_egl_surface_from_drawable(read);
   eglMakeCurrent(edpy, dsurf, rsurf, (EGLContext)context);
}

PUBLIC Bool glXQueryExtension(Display *dpy, int *error_base, int *event_base)
{  
   /* We does both query X Server to get an extension  and query EGL about supporting X11
    * or xcb platftorms */
   EGLDisplay edpy = EGL_NO_DISPLAY;
   edpy = eglGetPlatformDisplay(EGL_PLATFORM_XCB_EXT, XGetXCBConnection(dpy), NULL);
   if(edpy == EGL_NO_DISPLAY)
      edpy = eglGetPlatformDisplay(EGL_PLATFORM_X11_KHR, dpy, NULL);
   bool has_egl = edpy != EGL_NO_DISPLAY;
   int major;
   bool has_ext __glXQueryExtension(dpy, &major, error_base, event_base);
   return has_egl && has_ext;
}


PUBLIC Bool glXQueryVersion(Display *dpy, int *major, int *minor)
{
   /* Always return 1.4, because we use X Server only for errors and init */
    if (major) {
        *major = 1;
    }
    if (minor) {
        *minor = 4;
    }

    return True;
}


PUBLIC void glXSwapBuffers(Display *dpy, GLXDrawable drawable)
{
   EGLDisplay edpy = get_egl_display_from_display(dpy);
   EGLSurface surf = get_egl_surface_from_drawable(drawable);
   eglSwapBuffers(edpy, surf);
}


PUBLIC void glXUseXFont(Font font, int first, int count, int list_base)
{
   Display* dpy = get_display_from_egl_display(edpy);
   direct_glXUseXFont(dpy, DefaultScreenOfDisplay(dpy), font, count, list_base);
}


PUBLIC void glXWaitGL(void)
{
   eglWaitClient();
}


PUBLIC void glXWaitX(void)
{
   eglWaitNative(EGL_CORE_NATIVE_ENGINE);
}

PUBLIC const char *glXGetClientString(Display *dpy, int name)
{
    EGLDisplay* edpy = eglGetDisplay(dpy);
    switch (name) {
    case GLX_VENDOR:
        return eglQueryString(edpy, EGL_VENDOR);
    case GLX_VERSION:
        return GLX_VERSION_STRING " (EGL Renderer)";
    case GLX_EXTENSIONS:
        return GLX_EGL_EXTENSIONS;
    default:
        return NULL;
    }
}

PUBLIC const char *glXQueryServerString(Display *dpy, int screen, int name)
{
   /* This is the same, because we do not use actual GLX*/
   return glXGetClientString(dpy, name);
}


PUBLIC const char *glXQueryExtensionsString(Display *dpy, int screen)
{
   return GLX_EGL_EXTENSIONS;
}

PUBLIC GLXFBConfig *glXChooseFBConfig(Display *dpy, int screen,
                                      const int *attrib_list, int *nelements)
{
}


PUBLIC GLXPbuffer glXCreatePbuffer(Display *dpy, GLXFBConfig config,
                            const int *attrib_list)
{
   EGLDisplay edpy = get_egl_display_from_display(dpy);
   int srgb;
   glXGetFBConfigAttrib(dpy, config, GLX_FRAMEBUFFER_SRGB_CAPABLE_ARB, &srgb);
}


PUBLIC GLXPixmap glXCreatePixmap(Display *dpy, GLXFBConfig config,
                          Pixmap pixmap, const int *attrib_list)
{

}


PUBLIC GLXWindow glXCreateWindow(Display *dpy, GLXFBConfig config,
                          Window win, const int *attrib_list)
{
}

static void __glXDestroyDrawable(Display* dpy, GLXDrawable draw)
{
   EGLDisplay edpy = get_egl_display_from_display(dpy);
   EGLSurface surf = get_egl_surface_from_drawable(draw);
   unset_drawable_for_surface(draw, surf);
   eglDestroySurface(edpy, surf);
}


PUBLIC void glXDestroyPbuffer(Display *dpy, GLXPbuffer pbuf)
{
   __glXDestroyDrawable(dpy,pbuf);
}


PUBLIC void glXDestroyPixmap(Display *dpy, GLXPixmap pixmap)
{
      __glXDestroyDrawable(dpy,pixmap);
}


PUBLIC void glXDestroyWindow(Display *dpy, GLXWindow win)
{
   __glXDestroyDrawable(dpy,win);
}

PUBLIC int glXGetFBConfigAttrib(Display *dpy, GLXFBConfig config,
                         int attribute, int *value)
{
   EGLDisplay edpy = get_egl_display_from_display(dpy);
   EGLConfig conf = get_egl_config_from_glxfbconfig(config);
   int egl_val;
   int egl_attr = EGL_CONFIG_ID;
   bool parse_egl_value = false;

   if(!value) return GLX_BAD_VALUE;

   switch(attribute)
   {
      case GLX_FBCONFIG_ID:
         egl_attr = EGL_CONFIG_ID;
         break;
      case GLX_BUFFER_SIZE:
         egl_attr = EGL_BUFFER_SIZE;
         break;
      case GLX_LEVEL:
         egl_attr = EGL_LEVEL;
         break;
      case GLX_DOUBLEBUFFER:
         *value = GLX_DONT_CARE;
         return Success;
      case GLX_STEREO:
         *value = GLX_DONT_CARE;
         return Success;
      case GLX_AUX_BUFFERS:
         *value = 0;
         return Success;
      case GLX_RED_SIZE:
         egl_attr = EGL_RED_SIZE;
         break;
      case GLX_GREEN_SIZE:
         egl_attr = EGL_GREEN_SIZE;
         break;
      case GLX_BLUE_SIZE:
         egl_attr = EGL_BLUE_SIZE;
         break;
      case GLX_ALPHA_SIZE:
         egl_attr = EGL_ALPHA_SIZE;
         break;
      case GLX_DEPTH_SIZE:
         egl_attr = EGL_DEPTH_SIZE;
         break;
      case GLX_STENCIL_SIZE:
         egl_attr = EGL_STENCIL_SIZE;
         break;
      case GLX_ACCUM_RED_SIZE:
      case GLX_ACCUM_GREEN_SIZE:
      case GLX_ACCUM_BLUE_SIZE:
      case GLX_ACCUM_ALPHA_SIZE:
         *value = 0;
         return Success;
      case GLX_RENDER_TYPE:
         *value = GLX_RGBA_BIT;
         return Success;
      case GLX_DRAWABLE_TYPE:
         parse_egl_value = true;
         egl_attr = EGL_RENDERABLE_TYPE;
         break;
      case GLX_X_RENDERABLE:
         egl_attr = EGL_NATIVE_RENDERABLE;
         break;
      case GLX_VISUAL_ID:
         egl_attr = EGL_NATIVE_VISUAL_ID;
         break;
      case GLX_X_VISUAL_TYPE:
         egl_attr = EGL_NATIVE_VISUAL_TYPE;
         break;
      case GLX_CONFIG_CAVEAT:
      case GLX_TRANSPARENT_TYPE:
         parse_egl_value = true;
         egl_attr = EGL_TRANSPARENT_TYPE;
         break;
      case GLX_TRANSPARENT_INDEX_VALUE:
      case GLX_TRANSPARENT_ALPHA_VALUE:
         *value = 0;
         return Success;
      case GLX_TRANSPARENT_RED_VALUE:
         egl_attr = EGL_TRANSPARENT_RED_VALUE;
         break;
      case GLX_TRANSPARENT_GREEN_VALUE:
         egl_attr = EGL_TRANSPARENT_GREEN_VALUE;
         break;
      case GLX_TRANSPARENT_BLUE_VALUE:
         egl_attr = EGL_TRANSPARENT_BLUE_VALUE;
         break;
      case GLX_MAX_PBUFFER_WIDTH:
         egl_attr = EGL_MAX_PBUFFER_WIDTH;
         break;
      case GLX_MAX_PBUFFER_HEIGHT:
         egl_attr = EGL_MAX_PBUFFER_HEIGHT;
         break;
      case GLX_MAX_PBUFFER_PIXELS:
         egl_attr = EGL_MAX_PBUFFER_PIXELS;
         break;
      case GLX_SAMPLE_BUFFERS:
         egl_attr = EGL_SAMPLE_BUFFERS;
         break;
      case GLX_SAMPLES:
         egl_attr = EGL_SAMPLES;
         break;
      case GLX_FRAMEBUFFER_SRGB_CAPABLE_EXT:
         parse_egl_value = true;
         break;
      default:
         return GLX_BAD_ATTRIBUTE;
   }
   EGLBoolean ret = eglGetConfigAttrib(edpy, conf, egl_attr, &egl_val);
   if(!ret)
      return __glXGetErrorFromEGLError(eglGetError());
   if(parse_egl_value)
   {
      temp_egl_val = 0;
      temp_egl_red = 0;
      temp_egl_green = 0;
      temp_egl_val = 0;
      switch(egl_attr)
      {
      case EGL_TRANSPARENT_TYPE:
         if(egl_val = EGL_TRANSPARENT_RGB)
            temp_egl_val = GLX_TRANSPARENT_RGB;
         temp_egl_val = GLX_NONE;
         break;
      case EGL_RENDERABLE_TYPE:
         if(egl_val | EGL_WINDOW_BIT)
            temp_egl_val |= GLX_WINDOW_BIT;
         if(egl_val | EGL_PIXMAP_BIT)
            temp_egl_val |= GLX_PIXMAP_BIT;
         if(egl_val | EGL_PBUFFER_BIT)
            temp_egl_val |= GLX_PBUFFER_BIT;
         break;
      case GLX_FRAMEBUFFER_SRGB_CAPABLE_EXT:
         EGLBoolean tmp_ret = eglGetConfigAttrib(edpy, conf, EGL_RED_SIZE, &tmp_egl_red);
         if(!tmp_ret)
            return __glXGetErrorFromEGLError(eglGetError());
         EGLBoolean tmp_ret = eglGetConfigAttrib(edpy, conf, EGL_GREEN_SIZE, &tmp_egl_green);
         if(!tmp_ret)
            return __glXGetErrorFromEGLError(eglGetError());
         EGLBoolean tmp_ret = eglGetConfigAttrib(edpy, conf, EGL_BLUE_SIZE, &tmp_egl_blue);
         if(!tmp_ret)
            return __glXGetErrorFromEGLError(eglGetError());
         tmp_egl_val = !!(tmp_egl_red + tmp_egl_green + tmp_egl_blue == 24);
      }
      egl_val = temp_egl_val;
   }
   *value = egl_val;
   return Success;
}


PUBLIC GLXFBConfig *glXGetFBConfigs(Display *dpy, int screen, int *nelements)
{
}


PUBLIC void glXGetSelectedEvent(Display *dpy, GLXDrawable draw,
                         unsigned long *event_mask)
{
}


PUBLIC XVisualInfo *glXGetVisualFromFBConfig(Display *dpy, GLXFBConfig config)
{
}


PUBLIC int glXQueryContext(Display *dpy, GLXContext context, int attribute, int *value)
{
}


PUBLIC void glXQueryDrawable(Display *dpy, GLXDrawable draw,
                      int attribute, unsigned int *value)
{
}


PUBLIC void glXSelectEvent(Display *dpy, GLXDrawable draw, unsigned long event_mask)
{
}

const __GLXlocalDispatchFunction LOCAL_GLX_DISPATCH_FUNCTIONS[] =
{
#define LOCAL_FUNC_TABLE_ENTRY(func) \
    { #func, (__GLXextFuncPtr)(func) },
        LOCAL_FUNC_TABLE_ENTRY(glXChooseFBConfig)
        LOCAL_FUNC_TABLE_ENTRY(glXChooseVisual)
        LOCAL_FUNC_TABLE_ENTRY(glXCopyContext)
        LOCAL_FUNC_TABLE_ENTRY(glXCreateContext)
        LOCAL_FUNC_TABLE_ENTRY(glXCreateGLXPixmap)
        LOCAL_FUNC_TABLE_ENTRY(glXCreateNewContext)
        LOCAL_FUNC_TABLE_ENTRY(glXCreatePbuffer)
        LOCAL_FUNC_TABLE_ENTRY(glXCreatePixmap)
        LOCAL_FUNC_TABLE_ENTRY(glXCreateWindow)
        LOCAL_FUNC_TABLE_ENTRY(glXDestroyContext)
        LOCAL_FUNC_TABLE_ENTRY(glXDestroyGLXPixmap)
        LOCAL_FUNC_TABLE_ENTRY(glXDestroyPbuffer)
        LOCAL_FUNC_TABLE_ENTRY(glXDestroyPixmap)
        LOCAL_FUNC_TABLE_ENTRY(glXDestroyWindow)
        LOCAL_FUNC_TABLE_ENTRY(glXGetClientString)
        LOCAL_FUNC_TABLE_ENTRY(glXGetConfig)
        LOCAL_FUNC_TABLE_ENTRY(glXGetCurrentContext)
        LOCAL_FUNC_TABLE_ENTRY(glXGetCurrentDisplay)
        LOCAL_FUNC_TABLE_ENTRY(glXGetCurrentDrawable)
        LOCAL_FUNC_TABLE_ENTRY(glXGetCurrentReadDrawable)
        LOCAL_FUNC_TABLE_ENTRY(glXGetFBConfigAttrib)
        LOCAL_FUNC_TABLE_ENTRY(glXGetFBConfigs)
        LOCAL_FUNC_TABLE_ENTRY(glXGetProcAddress)
        LOCAL_FUNC_TABLE_ENTRY(glXGetProcAddressARB)
        LOCAL_FUNC_TABLE_ENTRY(glXGetSelectedEvent)
        LOCAL_FUNC_TABLE_ENTRY(glXGetVisualFromFBConfig)
        LOCAL_FUNC_TABLE_ENTRY(glXIsDirect)
        LOCAL_FUNC_TABLE_ENTRY(glXMakeContextCurrent)
        LOCAL_FUNC_TABLE_ENTRY(glXMakeCurrent)
        LOCAL_FUNC_TABLE_ENTRY(glXQueryContext)
        LOCAL_FUNC_TABLE_ENTRY(glXQueryDrawable)
        LOCAL_FUNC_TABLE_ENTRY(glXQueryExtension)
        LOCAL_FUNC_TABLE_ENTRY(glXQueryExtensionsString)
        LOCAL_FUNC_TABLE_ENTRY(glXQueryServerString)
        LOCAL_FUNC_TABLE_ENTRY(glXQueryVersion)
        LOCAL_FUNC_TABLE_ENTRY(glXSelectEvent)
        LOCAL_FUNC_TABLE_ENTRY(glXSwapBuffers)
        LOCAL_FUNC_TABLE_ENTRY(glXUseXFont)
        LOCAL_FUNC_TABLE_ENTRY(glXWaitGL)
        LOCAL_FUNC_TABLE_ENTRY(glXWaitX)
        LOCAL_FUNC_TABLE_ENTRY(glXCreateContextAttribsARB)
#undef LOCAL_FUNC_TABLE_ENTRY
    { NULL, NULL }
};

PUBLIC __GLXextFuncPtr glXGetProcAddressARB(const GLubyte *procName)
{

    return glXGetProcAddress(procName);
}

PUBLIC __GLXextFuncPtr glXGetProcAddress(const GLubyte *procName)
{
    __GLXextFuncPtr addr = NULL;

    if (procName[0] == 'g' && procName[1] == 'l' && procName[2] == 'X') {
        // This looks like a GLX function, so, use internal dispatch.
        addr = __glXGetGLXDispatchAddress(procName);
    } else {
        addr = eglGetProcAddress((const char *) procName);
    }

    return addr;
}

PUBLIC const __glXGLCoreFunctions __GLXGL_CORE_FUNCTIONS = {
    glXChooseFBConfig,
    glXChooseVisual,
    glXCopyContext,
    glXCreateContext,
    glXCreateGLXPixmap,
    glXCreateNewContext,
    glXCreatePbuffer,
    glXCreatePixmap,
    glXCreateWindow,
    glXDestroyContext,
    glXDestroyGLXPixmap,
    glXDestroyPbuffer,
    glXDestroyPixmap,
    glXDestroyWindow,
    glXGetClientString,
    glXGetConfig,
    glXGetCurrentContext,
    glXGetCurrentDrawable,
    glXGetCurrentReadDrawable,
    glXGetFBConfigAttrib,
    glXGetFBConfigs,
    glXGetProcAddress,
    glXGetProcAddressARB,
    glXGetSelectedEvent,
    glXGetVisualFromFBConfig,
    glXIsDirect,
    glXMakeContextCurrent,
    glXMakeCurrent,
    glXQueryContext,
    glXQueryDrawable,
    glXQueryExtension,
    glXQueryExtensionsString,
    glXQueryServerString,
    glXQueryVersion,
    glXSelectEvent,
    glXSwapBuffers,
    glXUseXFont,
    glXWaitGL,
    glXWaitX,
};

