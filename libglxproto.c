/*
 * Copyright (c) 2016, NVIDIA CORPORATION.
 * SPDX-License-Identifier: MIT
 */

#include <X11/Xlibint.h>


#include <GL/glx.h>
#include <GL/glxproto.h>

#include <EGL/egl.h>

#include "libglxproto.h"

/* TODO: xcb */

/*!
 * Reads a reply from the server, including any additional data.
 *
 * This function will read a reply from the server, and it will optionally
 * allocate a buffer and read any variable-length data in the reply.
 *
 * If \c replyData is not \c NULL and the reply contains additional data, then
 * this function will allocate a buffer and read the data into it.
 *
 * If \c replyData is \c NULL, then the caller must read any data in the reply.
 *
 * If the last request generates an error, then it will return the error code,
 * but it will not call the normal X error handler.
 *
 * \param dpyInfo The display connection.
 * \param[out] reply Returns the reply structure.
 * \param[out] replyData If not \c NULL, returns any additional reply data.
 * \return \c Success on a successful reply. If the server sent back an error,
 * then the error code is returned. If something else fails, then -1 is
 * returned.
 */
static Status ReadReply(__GLXdisplayInfo *dpyInfo, xReply *reply, void **replyData)
{
    Display *dpy = dpyInfo->dpy;
    _XAsyncHandler async;
    _XAsyncErrorState state = {};
    Status error = Success;

    state.min_sequence_number = state.max_sequence_number = dpy->request;
    state.major_opcode = dpyInfo->glxMajorOpcode;
    async.next = dpy->async_handlers;
    async.handler = _XAsyncErrorHandler;
    async.data = (XPointer) &state;
    dpy->async_handlers = &async;

    if (!_XReply(dpy, reply, 0, False)) {
        error = -1;
    }
    DeqAsyncHandler(dpy, &async);

    if (state.error_count) {
        error = state.last_error_received;
        if (error == Success) {
            assert(error != Success);
            error = -1;
        }
    }

    if (replyData != NULL) {
        void *data = NULL;
        if (error == Success && reply->generic.length > 0) {
            int length = reply->generic.length * 4;
            data = malloc(length);
            if (data != NULL) {
                _XRead(dpyInfo->dpy, (char *) data, length);
            } else {
                _XEatData(dpyInfo->dpy, length);
                error = -1;
            }
        }
        *replyData = data;;
    }

    return error;
}

char *__glXQueryServerString(__GLXdisplayInfo *dpyInfo, int screen, int name)
{
    Display *dpy = dpyInfo->dpy;
    xGLXQueryServerStringReq *req;
    xGLXSingleReply rep;
    char *ret = NULL;

    if (!dpyInfo->glxSupported) {
        return NULL;
    }

    LockDisplay(dpy);

    GetReq(GLXQueryServerString, req);
    req->reqType = dpyInfo->glxMajorOpcode;
    req->glxCode = X_GLXQueryServerString;
    req->screen = screen;
    req->name = name;

    ReadReply(dpyInfo, (xReply *) &rep, (void **) &ret);

    UnlockDisplay(dpy);
    SyncHandle();

    return ret;
}
Bool __glXQueryExtension(Display *dpy, int* major_base, int *error_base, int *event_base)
{
    /*
     * There isn't enough information to dispatch to a vendor's
     * implementation, so handle the request here.
     */
    int major, event, error;
    Bool ret = XQueryExtension(dpy, "GLX", &major, &event, &error);
    if (ret) {
         if (major_base) {
            *major_base = major;
        }
        if (error_base) {
            *error_base = error;
        }
        if (event_base) {
            *event_base = event;
        }
    }
    return ret;
}

void __glXSendGlxError(Display *dpy, CARD16 minorCode, CARD8 errorCode,
	bool x11Error)
{
	xError error;
	int majorCode, errorBase, dummy;

   bool avail = __glXQueryExtension(dpy, &majorCode, &errorBase, &dummy);

   if(!avail)
      return;

	LockDisplay(dpy);

	error.type = X_Error;
	error.errorCode = x11Error ? errorCode : errorBase + errorCode;
	error.sequenceNumber = dpy->request;
	error.resourceID = 0;
	error.minorCode = minorCode;
	error.majorCode = majorCode;
	_XError(dpy, &error);

	UnlockDisplay(dpy);
}

int __glXGetErrorFromEGLError(EGLint eglError)
{
   switch(eglError)
   {
      case EGL_SUCCESS:  return Success;
      case EGL_BAD_ACCESS:  return BadAccess;
      case EGL_BAD_ALLOC:  return BadAlloc;
      case EGL_BAD_CONFIG:  return GLXBadFBConfig;
      case EGL_BAD_CONTEXT:  return GLXBadContext;
      case EGL_BAD_CURRENT_SURFACE:  return GLXBadCurrentDrawable;
      case EGL_BAD_MATCH:  return BadMatch;
      case EGL_BAD_NATIVE_PIXMAP:  return GLXBadPixmap;
      case EGL_BAD_NATIVE_WINDOW:  return GLXBadWindow;
      case EGL_BAD_PARAMETER:  return BadValue;
      case EGL_BAD_SURFACE:  return GLXBadDrawable;
      default:  return -1;
   }
}

static bool __isX11Error(EGLint eglError)
{
   switch(eglError)
   {
      case EGL_SUCCESS:  return true;
      case EGL_BAD_ACCESS:  return true;
      case EGL_BAD_ALLOC:  return true;
      case EGL_BAD_MATCH:  return true;
      case EGL_BAD_PARAMETER:  return true;
      default:  return false;
   }
}

void __glXSendEGLError(Display *dpy, CARD16 minorCode, EGLint eglError)
{
   bool isX11 = __isX11Error(eglError);
   int glXErrorCode = __glXGetErrorFromEGLError(eglError);
   __glXSendGlxError(dpy, minorCode, glXErrorCode, isX11);
}


