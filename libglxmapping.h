/*
 * Copyright (c) 2013, NVIDIA CORPORATION.
 * SPDX-License-Identifier: MIT
 */
#if !defined(__LIB_GLX_MAPPING_H)
#define __LIB_GLX_MAPPING_H

#include <GL/glx.h>

#define GLX_CLIENT_STRING_LAST_ATTRIB GLX_EXTENSIONS
#define MAX_ATTRIBS  256

typedef void (* __GLXextFuncPtr)(void);

/*!
 * Structure containing per-display information.
 */
typedef struct __GLXdisplayInfoRec {
    Display *dpy;

    char *clientStrings[GLX_CLIENT_STRING_LAST_ATTRIB];

    /// True if the server supports the GLX extension.
    Bool glxSupported;

    /// The major opcode for GLX, if it's supported.
    int glxMajorOpcode;
    int glxFirstError;

} __GLXdisplayInfo;

typedef struct __GLXlocalDispatchFunctionRec {
    const char *name;
    __GLXextFuncPtr addr;
} __GLXlocalDispatchFunction;

/*!
 * A NULL-termianted list of GLX dispatch functions that are implemented in
 * libGLX instead of in any vendor library.
 */
extern const __GLXlocalDispatchFunction LOCAL_GLX_DISPATCH_FUNCTIONS[];

#endif /* __LIB_GLX_MAPPING_H */
