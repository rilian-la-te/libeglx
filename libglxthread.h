/*
 * Copyright (c) 2013, NVIDIA CORPORATION.
 * SPDX-License-Identifier: MIT
 */

#ifndef __LIBGLX_THREAD_H__
#define __LIBGLX_THREAD_H__

#define EGLX_THREAD_LOCAL(name, type, initValue) \
static pthread_key_t get##name##Key(void) \
{ \
	static pthread_key_t key; \
	static bool init = false; \
	if(!init) \
	{ \
		if(pthread_key_create(&key, NULL)) \
		{ \
			printf("[EGLX] ERROR: pthread_key_create() for " #name " failed.\n"); \
			pthread_exit(1); \
		} \
		pthread_setspecific(key, (const void *)initValue); \
		init = true; \
	} \
	return key; \
} \
\
type get##name(void) \
{ \
	return (type)pthread_getspecific(get##name##Key()); \
} \
\
void set##name(type value) \
{ \
	pthread_setspecific(get##name##Key(), (const void *)value); \
}

#endif
