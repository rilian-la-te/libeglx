/*
 * Copyright (c) 2016, NVIDIA CORPORATION.
 * SPDX-License-Identifier: MIT
 */

#ifndef LIBGLXPROTO_H
#define LIBGLXPROTO_H

#include <stdbool.h>

#include <X11/Xmd.h>

#include <EGL/eglplatform.h>

/*!
 * Functions to handle various GLX protocol requests.
 */

#include "libglxmapping.h"

/*!
 * Sends a glXQueryServerString request. If an error occurs, then it will
 * return \c NULL, but won't call the X error handler.
 *
 * \param dpyInfo The display connection.
 * \param screen The screen number.
 * \param name The name enum to request.
 * \return The string, or \c NULL on error. The caller must free the string
 * using \c free.
 */
char *__glXQueryServerString(__GLXdisplayInfo *dpyInfo, int screen, int name);

/*!
 * Looks up the screen number for a drawable.
 *
 * Note that if the drawable is valid, but server doesn't send a screen number,
 * then that means the server doesn't support the GLX_EXT_libglvnd extension.
 * In that case, this function will return zero, since we'll be using the same
 * screen for every drawable anyway.
 *
 * \param dpyInfo The display connection.
 * \param drawable The drawable to query.
 * \return The screen number for the drawable, or -1 on error.
 */
int __glXGetDrawableScreen(__GLXdisplayInfo *dpyInfo, GLXDrawable drawable);

int
direct_glXUseXFont(Display* dpy, int screen, Font font, int first, int count, int listbase);

Bool __glXQueryExtension(Display *dpy, int* major_base, int *error_base, int *event_base);

void __glXSendGlxError(Display *dpy, CARD16 minorCode, CARD8 errorCode,
	bool x11Error);
void __glXSendEGLError(Display *dpy, CARD16 minorCode, EGLint eglError);
int __glXGetErrorFromEGLError(EGLint eglError);

#endif // LIBGLXPROTO_H
